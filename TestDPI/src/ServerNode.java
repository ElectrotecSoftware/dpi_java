import components.DPIModel;
import components.DPINode;
import components.JTreeTable;
import components.TreeTableModelAdapter;
import electrotec.DPClt;
import electrotec.DPEvents;
import electrotec.DPNode;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JScrollPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Manel
 */
class ServerNode implements DPEvents
{
    String      name;
    String      ip;
    int         port;
    ServerPanel panel;
    DPClt       client;
    JTreeTable  treeTable ;
    DPIModel    model;
    
    ServerNode(String value)
    {
        // Propiedades de la entrada
        String[] values = value.split("#");
        name = values[0];
        ip   = values[1];
        port = Integer.valueOf(values[2]);
        
        // Conexión con el DPI
        client = new DPClt();
        client.addEventListener(this);
                      
        // Interacción con el usuario
        panel = new ServerPanel();
        panel.serverIP.setValue(ip);
        panel.serverName.setText(name);
        panel.serverPort.setValue(port);
        
        // Añadimos la tabla arbol
        model     = new DPIModel();
        treeTable = new JTreeTable(model);  
        treeTable.setTreeCellRenderer(new DPINode());
//        treeTable = new TreeTable(buildRoot());  
        treeTable.setRowHeight( 34 );
        treeTable.setRootVisible(true);
        
        panel.treeTable.add(new JScrollPane(treeTable),  java.awt.BorderLayout.CENTER);
        
        // Captura de acciones
        panel.toggleConnection.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toggleConnectionActionPerformed(evt);
            }
        });  
        
        panel.networkRequest.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendNetworkRequest(evt);
            }
        });  
        
        panel.serial.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendVersion(evt);
            }
        });      
        
        panel.openSession.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSession(evt);
            }
        });        
        
        panel.printDisplay.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendPrintDisplay(evt);
            }

        });    
        
        panel.serverIP.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ip = panel.serverIP.getText();
            }
        });          
               
        
        setPanelEnabled(false);
    }
    
//    private static TreeTableNode buildRoot ()
//    {
//        DefaultTreeTableNode root  = new DefaultTreeTableNode( new DPINode(), false);
//        DefaultTreeTableNode node1 = new DefaultTreeTableNode( new DPINode( 1, "channel 1" ) , false);
//        DefaultTreeTableNode node2 = new DefaultTreeTableNode( new DPINode( 2, "channel 2" ) , false);
//        DefaultTreeTableNode subNode1 = new DefaultTreeTableNode("Ignore this node", false);
//        node1.add(subNode1);
//        subNode1.add(new DefaultTreeTableNode("SubNode1-1", false));
//        subNode1 = new DefaultTreeTableNode("Step 2. Open This node and then Step 3. Click checkbox ->", false);
//        node1.add(subNode1);
//        subNode1.add(new DefaultTreeTableNode("SubNode2-1", false));
//        root.add(node1);
//        root.add(node2);
//        return root;
//    }
    
    private void setServerEnabled(boolean b )
    {
        panel.serverIP.setEnabled(b);            
        panel.serverName.setEnabled(b);
        panel.serverPort.setEnabled(b);        
    }
    
    private void setPanelEnabled( boolean b)
    {
        panel.networkRequest.setEnabled(b);
        panel.serial.setEnabled(b);
        panel.openSession.setEnabled(b);
        panel.cbColor.setEnabled(b);
        panel.cbKeySound.setEnabled(b);
        panel.cbMakeBeep.setEnabled(b);
        panel.printDisplay.setEnabled(b);
        panel.cbBlink.setEnabled(b);     
        panel.isAR.setEnabled(b);     
        
    }
    
    private void sendNetworkRequest(ActionEvent evt) 
    {
        try {
            client.sendNetworkDistributionRequest();
            append("Send network distribution request ...\n");
        } catch (Exception ex) {
            append( ex.toString()+"\n");
        }
    }    
    
    private void sendVersion(ActionEvent evt) 
    {
        try {
            client.sendServerVersionRequest();
            append("Send version request ...\n");
        } catch (Exception ex) {
            append( ex.toString()+"\n");
        }
    }  
    
    private void openSession(ActionEvent evt) 
    {
        try {
            client.openSession();
            append("Send session request ...\n");
        } catch (Exception ex) {
            append( ex.toString()+"\n");
        }
    }
    
    private void toggleConnectionActionPerformed(ActionEvent evt) {
        if( panel.toggleConnection.isSelected() ){
            String value =panel.serverPort.getText();
            
            try {
                if( !ip.equals(panel.serverIP.getText()) )
                    ip = panel.serverIP.getText();
                
                if( port != Integer.valueOf( value ))
                    port = Integer.valueOf( value );
                
                append("Connecting to  "+ip+":"+port+"...\n");  

                client.connect(ip, port);
                // Creacion de la hebra
                (new Thread(client)).start();
                append("Thread start \n"); 
            } catch (Exception ex) {
                append( ex.toString()+"\n");
            }
        }else{
            try {
                append("Diconnecting from  "+ip+":"+port+"...\n");                  
                client.close();
            } catch (Exception ex) {
                append( ex.toString()+"\n");
            }            
        }            
    }
    
    private void sendPrintDisplay(ActionEvent evt) 
    {
        try {
            int value = 0;
            String nodeId="1", channel="1";
            Object obj = panel.nodeNumber.getValue();
            if( obj != null )
                value = Integer.valueOf(obj.toString());
            
            if( value == 0 ){
                int row = treeTable.getSelectedRow();
                DPINode nodeRow  = (DPINode)treeTable.getValueAt(row, 1);
                if( nodeRow != null ){
                    nodeId = nodeRow.toString();
                    channel = "" + (nodeRow.getChannel()+1);
                }
            }else{
                nodeId  = ""+value;
                nodeId  = "000".substring(nodeId.length())+nodeId;
                channel = "1";
                
            }
                
            String text = panel.text.getText();
            
            if( panel.isAR.isSelected() ){
                client.printDisplayAR( nodeId,
                        channel,
                        text,
                        DPClt.LEDColor.getEnum(panel.cbColor.getSelectedItem().toString().toUpperCase()),
                        DPClt.BlinkMode.getEnum(panel.cbBlink.getSelectedItem().toString().toUpperCase()),
                        DPClt.KeySound.getEnum(panel.cbKeySound.getSelectedItem().toString().toUpperCase()),
                        DPClt.MakeBeep.getEnum(panel.cbMakeBeep.getSelectedItem().toString().toUpperCase()));
                append( "printDisplayAR:" );
            }else{
                client.printDisplay( nodeId,
                        channel,
                        text,
                        DPClt.LEDColor.getEnum(panel.cbColor.getSelectedItem().toString().toUpperCase()),
                        DPClt.BlinkMode.getEnum(panel.cbBlink.getSelectedItem().toString().toUpperCase()),
                        DPClt.KeySound.getEnum(panel.cbKeySound.getSelectedItem().toString().toUpperCase()),
                        DPClt.MakeBeep.getEnum(panel.cbMakeBeep.getSelectedItem().toString().toUpperCase()));
                append( "printDisplay:" );
            }
            append( nodeId+" "+channel+" "+text+","+
                    panel.cbColor.getSelectedItem().toString().toUpperCase()+
                    " ("+DPClt.LEDColor.getEnum(panel.cbColor.getSelectedItem().toString().toUpperCase())+")"+","+
                    panel.cbBlink.getSelectedItem().toString().toUpperCase()+
                    " ("+DPClt.BlinkMode.getEnum(panel.cbBlink.getSelectedItem().toString().toUpperCase())+")"+","+
                    panel.cbKeySound.getSelectedItem().toString().toUpperCase()+
                    " ("+DPClt.KeySound.getEnum(panel.cbKeySound.getSelectedItem().toString().toUpperCase())+")"+","+
                    panel.cbMakeBeep.getSelectedItem().toString().toUpperCase()+ 
                    " ("+DPClt.MakeBeep.getEnum(panel.cbMakeBeep.getSelectedItem().toString().toUpperCase())+")"+
                    "\n");
        } catch (Exception ex) {
            append( ex.toString()+"\n");
        }
    }    
    
    
    public void append( String text )
    {
        panel.jLogArea.append(text);
        
        Dimension tamaño = panel.jLogArea.getSize();
        Point p = new Point( 0, tamaño.height);
        panel.jScrollPane1.getViewport().setViewPosition(p);    
    }
    
    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public void onConnect() 
    {
        try {
            append("Connected to  "+ip+":"+port+".\n");      
            setServerEnabled(false);
            setPanelEnabled(true);
            model.setIP(ip,port);
            panel.repaint();
        } catch (Exception ex) {
            append( ex.toString()+"\n");
        }
    }

    @Override
    public void onDisconnect() 
    {
        append("Diconnected from  "+ip+":"+port+".\n"); 
        append("Thread stop.\n"); 
        panel.toggleConnection.setSelected(false);
        setServerEnabled(true);
        setPanelEnabled(false);        
        
    }

    @Override
    public void onVersion(String version) 
    {
        append("Server version is "+version+".\n"); 
    }

    @Override
    public void onOpenSession(String workingSessions) 
    {
        if( workingSessions.equals("9") )
            append("no sessions available\n");    
        else{
            append("Server session is "+workingSessions+".\n");    
        }
    }

    @Override
    public void onError(Exception error) {
        append( Arrays.toString(error.getStackTrace())+" "+error.toString()+"\n");
    }

    @Override
    public void onAlarmReception(String nodeId, String channel, int errorNumber, String errorDescription) 
    {
        append( "node "+nodeId+ " channel "+channel+" "+ errorDescription+"\n");
    }

    @Override
    public void onPrintDisplayACK(ByteBuffer msg, String nodeId, String channel, int errorNumber, String errorDescription) 
    {
        if( errorNumber == 9 )            
            append( "printDisplayAR:"+nodeId+" "+channel+" "+errorDescription+"\n");
        else
            append( "printDisplayAR:"+nodeId+" "+channel+" error "+errorNumber+" "+ errorDescription+"\n");
    }

    @Override
    public void onSetNetworkACK(int errorNumber, String errorDescription) 
    {
        append( "Error "+errorNumber+" "+ errorDescription+"\n");
    }

    @Override
    public void onGetMemory(String nodeId, String memory) {
        
    }

    @Override
    public void onKeyStroke(String nodeId, String channel, String displayType, String nButton, String keystroke) 
    {
        append( "Key Pressed, node "+nodeId+ " channel "+channel+" type "+ displayType+ " key "+ keystroke+ "\n");
    }
    
    @Override
    public void onScanReception(String nodeId, String channel, String scan ) 
    {
        append( "Scan code, node "+nodeId+ " channel "+channel+" scan ["+ scan+ "]\n");
    }    

    @Override
    public void onNetworkDistribution(ArrayList<DPNode> channel1, ArrayList<DPNode> channel2) 
    {
        ArrayList<DPINode> nodes = new ArrayList<>(); 
        append( "channel 1 \n" );
        model.initChannel(0);
        model.initChannel(1);

        for( DPNode node:channel1 ){
            nodes.add(model.addNode( 0, node ));
            append( "\t"+node.getNodeId()+","+node.getType()+","+node.getState()+"\n" );
        }
        append( "channel 2 \n" );
        for( DPNode node:channel2 ){
            nodes.add(model.addNode( 1, node ));            
            append( "\t"+node.getNodeId()+","+node.getType()+","+node.getState()+"\n" );
        }  
        
        model.repaint(nodes.toArray());
        TreeTableModelAdapter modelAdapter = (TreeTableModelAdapter)treeTable.getModel(); 
        modelAdapter.fireTableDataChanged();
        panel.repaint();
    }
}
