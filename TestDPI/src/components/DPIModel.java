package components;
import electrotec.DPNode;
import java.awt.event.*;

        
/**
 * ProcessModel is a TreeTableModel representing a hierarchical file 
 * system. Nodes in the ProcessModel are ProcessNodes which, when they 
 * are directory nodes, cache their children to avoid repeatedly querying 
 * the real file system. 
 * 
 * @version %I% %G%
 *
 * @author Philip Milne
 * @author Scott Violet
 */

public class DPIModel extends AbstractTreeTableModel  implements TreeTableModel 
{
    static protected String[] cNames = {"Name", "Node Id", "Type", "State" };
    static protected Class[]  cTypes = {TreeTableModel.class, DPINode.class, String.class, String.class };
    protected DPINode  rootNode = null;
 
    public DPIModel(  ) 
    { 
        super(new DPINode());        
        ((DPINode)root).addChannels();        
    }

    @Override
    public int getChildCount(Object node) 
    { 
        int ret=0;
        
        if ( node instanceof DPINode )
            ret = ((DPINode)node).getChildCount();
        
	return ret;
    }

    @Override
    public Object getChild(Object node, int i) 
    { 
        Object obj=null;
        
        if ( node instanceof DPINode )
            obj = ((DPINode)node).getChild(i);
        
        return obj; 
    }
 
    @Override
    public boolean isLeaf(Object node) 
    { 
        return getChildCount(node) == 0;
    }

    @Override
    public int getColumnCount() {
	return cNames.length;
    }

    @Override
    public String getColumnName(int column) {
	return cNames[column];
    }

    @Override
    public Class getColumnClass(int column) {
	return cTypes[column];
    }
 
    @Override
    public Object getValueAt(Object node, int column) {
	DPINode pNode = (DPINode)node; 

        if( pNode != null ){
            switch(column) {
                case 0:
                    return "";
                case 1:
                    return pNode ;
                case 2:
                    return pNode.getType();
                case 3:
                    return pNode.getStatus();
                case 4:
                    return pNode.getChannel();
            }   
        }
   
	return null; 

    }

    public void repaint(Object[] objects)
    {
        Object[] path=new Object[1];
        path[0]=root;
        this.fireTreeStructureChanged( this, path, null,  objects );
    }
    
    public void setIP(String ip, int port) {
        ((DPINode)root).setDescription(ip+":"+port);
    }

    public void initChannel(int i) 
    {
        ((DPINode)root).getChild(i).clearChilds();
    }

    public DPINode addNode(int i, DPNode node) {
        return ((DPINode)root).getChild(i).addNode(node);
    }


}



