
import java.awt.CardLayout;
import java.util.Enumeration;
import java.util.ResourceBundle;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;

/**
 * This class implements the firing of events.
 * @author      MJM
 * @version     1.0
 * @since       1.0
 */
public class TestAdapter 
{
   
    private static final ResourceBundle serversResource = ResourceBundle.getBundle("servers");
    
    TestPanel panel;
    
    TestAdapter( TestPanel panel )
    {
        this.panel =panel;
        // Carga de los datos de los servidores
        Enumeration bundleKeys = serversResource.getKeys();
        DefaultListModel model = new DefaultListModel();
        String key, value;
        ServerNode node;
        while (bundleKeys.hasMoreElements()) {
            key = (String)bundleKeys.nextElement();
            value = serversResource.getString(key);
            node = new ServerNode(value);
            model.addElement(node);
            panel.serversPanel.add(node.panel,node.toString());            
        }

        panel.serverList.setModel(model);      
        if( panel.serverList.getModel().getSize() > 0 ){
            panel.serverList.setSelectedIndex(0);
                        
        }                
        
        panel.serverList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                serverListValueChanged(evt);
            }


        });        
    }

    public void serverListValueChanged(ListSelectionEvent evt) {
        if( !evt.getValueIsAdjusting() ){
            String value = panel.serverList.getModel().getElementAt(panel.serverList.getSelectedIndex()).toString();
           ((CardLayout)panel.serversPanel.getLayout()).show(panel.serversPanel, value );        
        }
    }
}
