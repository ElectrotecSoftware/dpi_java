/*
 * ProcessNode.java
 *
 * Created on 1 de diciembre de 2006, 11:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */


import electrotec.DPNode;
import java.awt.Component;
import javax.swing.*;
import javax.swing.tree.*;
import java.util.ArrayList;


/* 
 */
public class DPINode extends DefaultMutableTreeNode  
{ 
    
    String description;
    String position;
    String type;
    String state;
    int    channel;
    
    static private Icon DPI      = null;
    static private Icon channel1 = null;
    static private Icon channel2 = null;
    static private Icon DP1      = null;
    static private Icon DPZ1     = null;
    static private Icon DM1      = null;
    static private Icon LC1      = null;

    private boolean isDPI      = false;
    private boolean isChannel1 = false;
    private boolean isChannel2 = false;
    private boolean isDP1      = false;
    private boolean isDPZ1     = false;  
    private boolean isDM1      = false;    
    private boolean isLC1      = false;

    private ArrayList<DPINode> childs= new ArrayList<>(); 
    

    public DPINode() 
    { 
        try{
            if( DPI      == null ) DPI      = new javax.swing.ImageIcon(getClass().getResource("/imgs/DPI.png"));
            if( channel1 == null ) channel1 = new javax.swing.ImageIcon(getClass().getResource("/imgs/channel1.png"));
            if( channel2 == null ) channel2 = new javax.swing.ImageIcon(getClass().getResource("/imgs/channel2.png"));
            if( DP1      == null ) DP1      = new javax.swing.ImageIcon(getClass().getResource("/imgs/DP1.png"));
            if( DPZ1     == null ) DPZ1     = new javax.swing.ImageIcon(getClass().getResource("/imgs/DPZ1.png"));
            if( DM1      == null ) DM1      = new javax.swing.ImageIcon(getClass().getResource("/imgs/DPM1.png"));
            if( LC1      == null ) LC1      = new javax.swing.ImageIcon(getClass().getResource("/imgs/LC1.png"));
            
        }catch(Exception e){
            e.printStackTrace();
        }
       
        isDPI = true;       
        description = "DPI";
    }    
    
    public DPINode( int numChannel, String description )
    {
        if( numChannel == 1)
            isChannel1 = true;
        if( numChannel == 2)
            isChannel2 = true; 

        this.description = description;
    }
    
    public DPINode( DPNode node )
    {
        this.description = node.getNodeId();
        this.type        = node.getType().toString();
        this.state       = node.getState().toString();
        this.channel     = node.getChannel();
        
        switch(node.getType().getFamily() ){
            case 1:
                isDP1 =true;
                break;
            case 3:
                isLC1 = true;
                break;
            case 2:
                isDPZ1 = true;
                break;
            case 4:
            case 0:
                isDM1 = true;
                break;
        }
            
    }    
    
    synchronized public void addChannels(  )
    {
        childs.clear();
       
        childs.add( new DPINode( 1, "channel 1" ) );
        childs.add( new DPINode( 2, "channel 2" ) );
    }
    

    public DPINode addNode(DPNode node) 
    {
        DPINode tmp=new DPINode( node );
        childs.add( tmp );        
        return tmp;
    }    
    
    public DPINode getChild(int col)
    {
        return childs.get(col);
    }
    
    
    public Component getTreeCellRendererComponent( JTree arbol, Object valor,boolean seleccionado,boolean expandido, boolean rama,int fila,boolean conFoco ) 
    {
        JLabel      label = new JLabel();
        Component comp = super.get
        
        if( valor instanceof DPINode  ){
            label.setToolTipText("");
            label.setText("");           
            
            if( ((DPINode)valor).isDPI ){
                label.setIcon(DPINode.DPI);
            }
            if( ((DPINode)valor).isChannel1 ){
                label.setIcon(DPINode.channel1);
            }  
            if( ((DPINode)valor).isChannel2 ){
                label.setIcon(DPINode.channel2);
            } 
            if( ((DPINode)valor).isDP1 ){
                label.setIcon(DPINode.DP1);
            }            
            if( ((DPINode)valor).isDPZ1 ){
                label.setIcon(DPINode.DPZ1);
            }   
            if( ((DPINode)valor).isDM1 ){
                label.setIcon(DPINode.DM1);
            }
            if( ((DPINode)valor).isLC1 ){
                label.setIcon(DPINode.LC1);
            }             
        }
        return label;
    }

    @Override
    public boolean isLeaf() 
    {
        return childs.isEmpty();
    }

    Object getPosition() 
    {
        return position;
    }

    Object getType() 
    {
        return type;
    }

    Object getStatus() 
    {
        return state;
    }
    
    public int getChannel()
    {
        return channel;
    }

    void setDescription(String string) 
    {
        description = string;
    }

    public Object getDescription()
    {
        return description;
    }
    
    @Override
    public String toString()
    {
        return description;
    }

    void clearChilds() 
    {
        this.childs.clear();
    }

    /**
     * Loads the children, caching the results in the children ivar.
     * @return 
     */
    protected Object[] getChildren() 
    {
        return childs.toArray();
    }    

    @Override
    public int getChildCount() 
    {
        return childs.size();
    }




}