import components.TreeTableModel;
import electrotec.DPNode;
import java.awt.event.*;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

        
/**
 * ProcessModel is a TreeTableModel representing a hierarchical file 
 * system. Nodes in the ProcessModel are ProcessNodes which, when they 
 * are directory nodes, cache their children to avoid repeatedly querying 
 * the real file system. 
 * 
 * @version %I% %G%
 *
 * @author Philip Milne
 * @author Scott Violet
 */

public class DPIModel  implements TreeTableModel 
{
    static protected String[] cNames = {"Name", "Node Id", "Type", "State" };
    static protected Class[]  cTypes = {TreeTableModel.class, DPINode.class, String.class, String.class };
    protected DPINode  rootNode = null;
 
    public DPIModel(  ) 
    { 
        rootNode = new DPINode();        
    }

    @Override
    public int getChildCount(Object node) 
    { 
        int ret=0;
        
        if ( node instanceof DPINode )
            ret = ((DPINode)node).getChildCount();
        
	return ret;
    }

    @Override
    public Object getChild(Object node, int i) 
    { 
        Object obj=null;
        
        if ( node instanceof DPINode )
            obj = ((DPINode)node).getChild(i);
        
        return obj; 
    }
 
    @Override
    public boolean isLeaf(Object node) 
    { 
        return getChildCount(node) == 0;
    }

    @Override
    public int getColumnCount() {
	return cNames.length;
    }

    @Override
    public String getColumnName(int column) {
	return cNames[column];
    }

    @Override
    public Class getColumnClass(int column) {
	return cTypes[column];
    }
 
/*    @Override
    public Object getValueAt(Object node, int column) {
	DPINode pNode = (DPINode)node; 

        if( pNode != null ){
            switch(column) {
                case 0:
                    return "";
                case 1:
                    return pNode ;
                case 2:
                    return pNode.getType();
                case 3:
                    return pNode.getStatus();
                case 4:
                    return pNode.getChannel();
            }   
        }
   
	return null; 

    }*/
    public void setIP(String ip, int port) {
        rootNode.setDescription(ip+":"+port);
    }

    public void initChannel(int i) 
    {
        rootNode.getChild(i).clearChilds();
    }

    public DPINode addNode(int i, DPNode node) {
        return rootNode.getChild(i).addNode(node);
    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}



