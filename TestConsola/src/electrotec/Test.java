package electrotec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PROGRAMADOR2
 */
public class Test implements DPEvents {

    DPClt   client;
    String  host;
    int     port;
    boolean exit=false; 
   
    
    public Test( String host, int port ){
        this.client = new DPClt();
        this.client.addEventListener(this);
        
        this.host = host;
        this.port = port;
    }
    
    public void run()
    {
        try {
            client.connect(host, port);
            
            client.run();
            
            client.close();
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void close()
    {
         try {
          
            client.close();
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    @Override
    public void onConnect() {
        System.out.println("Nos hemos conectado con el sistema, pedimos la version");
        try {
            client.sendServerVersionRequest();

        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onDisconnect() {
        System.out.println("Nos hemos desconectado con el sistema, ");
    }

    @Override
    public void onVersion(String version) {
        System.out.println("onVersion, "+version);
        System.out.println("abrimos sesion");
        try {
            client.openSession();
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onOpenSession(String workingSessions) {
        System.out.println("La sesion es "+workingSessions);
        try {
            client.printDisplay("001", "1", "hola", DPClt.LEDColor.Blue, DPClt.BlinkMode.noBlink, DPClt.KeySound.activate, DPClt.MakeBeep.noBeep);
            client.close();
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onError(Exception error) {
        System.out.println(error);
    }

    @Override
    public void onAlarmReception(String nodeId, String channel, int errorNumber, String errorDescription) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onPrintDisplayACK(ByteBuffer msg, String nodeId, String channel, int errorNumber, String errorDescription) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onSetNetworkACK(int errorNumber, String errorDescription) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onGetMemory(String nodeId, String memory) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onKeyStroke(String nodeId, String channel, String displayType, String nButton, String keystroke) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onScanReception(String nodeId, String channel, String scan) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }

    @Override
    public void onNetworkDistribution(ArrayList<DPNode> channel1, ArrayList<DPNode> channel2) {
        System.out.println("Nos hemos conectado con el sistema, ");
    }
}
