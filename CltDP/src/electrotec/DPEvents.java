/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package electrotec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.EventListener;

/**
 * This interface implements the events received by messages from the server.
 * @author      MJM
 * @version     1.0
 * @since       1.0
 */
public interface DPEvents extends EventListener {

    /**
     * The client has been connected to the server.
     * @see DPClt#connect(java.lang.String, int) 
     */
    void onConnect();
    
    /**
     * The client has been disconnected from the server.
     */    
    void onDisconnect();

    /**
     * The server return the protocol version.
     * @param version number of protocol version
     * @see DPClt#sendServerVersionRequest() 
     */
    void onVersion( String version );

    /**
     * The server return the number of current working sessions.
     * @param workingSessions, the server returns the number of current working sessions (0-5).
     *  If the session can’t be activated it returns a number
     * @see DPClt#openSession() 
     */
    void onOpenSession( String workingSessions );

    /**
     * The server return a telegram with unknown command or other error.
     * @param error is the exception object
     */
    void onError( Exception error );  
    
    /**
     * this event is sent to tell the kind of error produced and the involved node.
     * If the error does not involve a node, node 0 is sent.
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param errorNumber the value of errorNumber
     * @param errorDescription the error description
     * <P>
<table summary="">
     <tr><th>Err Number</th><th>Description</th></tr> 
     <tr><td>0x31</td><td>Comunication Ring on one channel is broken</td></tr>
     <tr><td>0x32</td><td>Node detected as disconnected</td></tr>
     <tr><td>0x33</td><td>Node detected with hardware problems</td></tr>
     <tr><td>0x34</td><td>Frame with wrong format</td></tr>
     <tr><td>0x35</td><td>Alarm when trying to send data to nodes not configured in DPI interface</td></tr>
     <tr><td>0x36</td><td>Impossible to transmit the frame to node</td></tr>
</table> 
     */
    void onAlarmReception(String nodeId, String channel, int errorNumber, String errorDescription);
    
    /**
     * The server returns acknowledge of Display print request.
     * If the error does not involve a node, node 0 is sent.
     * @param msg is the display print request
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param errorNumber the value of errorNumber
     * @param errorDescription the error description

     * 
     * <P>
<table summary="">
     <tr><th>Err Number</th><th>Description</th></tr> 
     <tr><td>0x39</td><td>ACK OK – Transmission was done</td></tr>
     <tr><td>0x32</td><td>ACK ERROR – Node detected as disconnected</td></tr>
     <tr><td>0x33</td><td>ACK ERROR – Node detected with hardware problems</td></tr>
     <tr><td>0x34</td><td>ACK ERROR – Frame with wrong format</td></tr>
     <tr><td>0x35</td><td>ACK ERROR – Alarm when trying to send data to nodes not configured in DPI interface</td></tr>
     <tr><td>0x36</td><td>ACK ERROR – Impossible to transmit frame to node</td></tr>
</table> 
     * @see DPClt#addQueueOut(java.nio.ByteBuffer)
     */
    void onPrintDisplayACK(ByteBuffer msg, String nodeId, String channel, int errorNumber, String errorDescription);    
    
    /**
     * The server returns acknowledge of set network distribution.
     * If the error does not involve a node, node 0 is sent.
     * @param errorNumber the value of errorNumber
     * @param errorDescription the error description 
     * <P>
<table summary="">
     <tr><th>Err Number</th><th>Description</th></tr> 
     <tr><td>0x39</td><td>ACK OK – Configuration transmitted OK</td></tr>
     <tr><td>0x34</td><td>ACK ERROR – Frame with wrong format</td></tr>
     <tr><td>0x36</td><td>ACK ERROR – Impossible to store content</td></tr>
</table> 
     * @see DPClt
     */
    void onSetNetworkACK( int errorNumber, String errorDescription);    
 
    /**
     * The server return 
     * @param nodeId identifies the network element
     * @param memory refers to the value (Decimal) of the six next bytes to the given memory address.
     */
    void onGetMemory( String nodeId, String memory );

    /**
     * The server return a keystroke
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param displayType type of display that sends the message
     * @param nButton down button number
     * @param keystroke symbol of the pressed key
     */
    void onKeyStroke(String nodeId, String channel, String displayType, String nButton, String keystroke);   
    
    /**
     * The server return a keystroke
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param scan  codebar
     */
    void onScanReception(String nodeId, String channel, String scan );       
    
    /**
     * The server return a network distribution
     * @param channel1 nodes in the channel1
     * @param channel2 nodes in the channel2
     * @see DPNode
     */    
    void onNetworkDistribution( ArrayList<DPNode> channel1, ArrayList<DPNode> channel2 );
}
