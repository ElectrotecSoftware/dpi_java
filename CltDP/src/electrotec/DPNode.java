/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2015 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2015 Sun Microsystems, Inc.
 */
package electrotec;

/**
 * This class implements the data of a network element.
 * @author      MJM
 * @version     1.0
 * @since       1.0
 */
public class DPNode {

    /**
     * Node State.
     */    
    public enum NodeState{
        /**
         * Unknown
         */
        unknown(0,"unknown"), 
        /**
         * Node initializing
         */
        init(1,"init"), 
        /**
         * Node in local mode
         */
        local(2,"local"), 
        /**
         * Node in normal state
         */
        normal(3,"normal"), 
        /**
         * Node in Error state
         */
        error(4,"error"),         
        /**
         * Node in programming state
         */
        programming(5,"programming");
           
        private final String state;
        private final String description;
        
        NodeState(int paramRelay, String desc){
            state = ""+((char)paramRelay+'0');
            description = desc;
        }

        @Override
        public String toString()
        {
            return description;
        }
        
        public String getValue()
        {
            return state;
        } 
        
        public static NodeState getEnum(String name)
        {
            if( name.equals("1"))
                return NodeState.init;
            
             if( name.equals("2"))
                return NodeState.local;           
 
             if( name.equals("3"))
                return NodeState.normal;      

             if( name.equals("4"))
                return NodeState.error;    
             
             if( name.equals("5"))
                return NodeState.programming;                 
             
            return unknown;
        }         

    }     
    
    /**
     * Node type.
     */    
    public enum NodeType{
        /**
         * Node ID not configured
         */
        unknown(0,"unknown", 0), 
        /**
         * Display 4 digits 14seg
         */
        DPA1(1,"DPA", 1), 
        /**
         * Display 12 digits 14seg
         */
        DPAZ1(2,"DPAZ1", 2), 
        /**
         * Display 4 digits dotmatrix
         */
        DPM1(3,"DPM1", 1), 
        /**
         * Display 12 digits dotmatrix
         */
        DPMZ1(4,"DPMZ1", 2), 
        /**
         * Display low cost 1 led 1 pushbutton
         */
        LC1(5,"LC1", 3),   
        /**
         * Interface LCI2/LCIN1
         */
        LCIN1(6,"LCI2/LCIN1",4),           
        /**
         * Interface DP/LC
         */
        DPW1(7,"DPW1",4),
        /**
         * Interface DPA2
         */
        DPA2(8,"DPA2",1);        

        public static NodeType getEnum(String name) 
        {
            if( name.equals("1"))
                return NodeType.DPA1;
            
             if( name.equals("2"))
                return NodeType.DPAZ1;           
 
             if( name.equals("3"))
                return NodeType.DPM1;      

             if( name.equals("4"))
                return NodeType.DPMZ1;    
             
             if( name.equals("5"))
                return NodeType.LC1;   
             
             if( name.equals("7"))
                return NodeType.DPW1;   
             
             if( name.equals("6"))
                return NodeType.LCIN1;      
             
             if( name.equals("8"))
                return NodeType.DPA2;                 
             
             return NodeType.unknown;
        }
           
        private String type;
        private String description;
        private int family;
        
        NodeType(int paramRelay, String desc, int family){
            type = ""+((char)paramRelay+'0');
            description = desc;
            this.family = family;
        }

        @Override
        public String toString()
        {
            return description;
        }
        
        public String getValue()
        {
            return type;
        }    
        
        public int getFamily()
        {
            return family;
        }

    } 

    /**
     * identifies the network element
     */    
    private String    nodeId;
    /**
     * identifies the status of the item
     */        
    private NodeState state; 
    /**
     * identifies the type of the item
     */    
    private NodeType  type;
    /**
     * identifies the channel of the item
     */    
    private int channel;    
    
    
    /**
     * @return the nodeId
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * @param nodeId the nodeId to set
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * @return the state
     */
    public NodeState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(NodeState state) {
        this.state = state;
    }

    /**
     * @return the type
     */
    public NodeType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(NodeType type) {
        this.type = type;
    }    
    
    /**
     * @param lastMsg the state to set
     */
    void setState(String lastMsg) 
    {
        state = NodeState.getEnum(lastMsg);
    }    
    
    /**
     * @param lastMsg the type to set
     */
    public void setType(String lastMsg) {
        this.type = NodeType.getEnum(lastMsg);
    }        

    /**
     * @return the channel
     */
    public int getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(int channel) {
        this.channel = channel;
    }
    
}
