/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2015 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2015 Sun Microsystems, Inc.
 */
package electrotec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the firing of events.
 * @author      MJM
 * @version     1.0
 * @since       1.0
 */
public class DPEventsAdapter {
    /**
     * list of listeners to receive notifications
     */
    private List<DPEvents>  listeners   = new ArrayList<>();     
    
    /**
     * lets you add a listener of events
     * @param listener object to be notified
     */
    public void addEventListener( DPEvents listener ) 
    {
        listeners.add(listener);
    }
    
    /**
     * lets you remove a listener of events
     * @param listener object to be notified
     */
    public void removeEventListener(DPEvents listener) 
    {
        listeners.remove(listener);
    }    
    
    /**
     * notifies listeners list of an error
     * @param error is the exception object
     */
    public void fireOnError( Exception error ) 
    {
        for( DPEvents listener: listeners ){
            listener.onError(error);
        }
    }       
    
    /**
     * notifies listeners list of server connection
     */
    public void fireOnConnect()
    {
        for( DPEvents listener: listeners ){
            listener.onConnect();
        }
    }       
    
    /**
     * notifies listeners list server disconnection
     */
    public void fireOnDisconnect()
    {
        for( DPEvents listener: listeners ){
            listener.onDisconnect();
        }
    }     
    
    /**
     * notifies the listener list server version
     * @param version of the server software.
     */
    public void fireOnVersion( String version  )
    {
        for( DPEvents listener: listeners ){
            listener.onVersion(version);
        }
    }       
    
    /**
     * notifies listeners list the session number assigned by the server
     * @param sessionId identifier given by the server can have up to six (0-5) sessions if all are occupied obtain a 9.
     */
    public void fireOnOpenSession( String sessionId  )
    {
        for( DPEvents listener: listeners ){
            listener.onOpenSession(sessionId);
        }
    }      
      
    /**
     * notifies listeners list the keystroke from a node
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param displayType type of display that sends the message
     * @param nButton down button number
     * @param keystroke symbol of the pressed key
     */
    public void fireOnKeyStroke( String nodeId, String channel, String displayType, String nButton, String keystroke)   
    {
        for( DPEvents listener: listeners ){
            listener.onKeyStroke( nodeId, channel, displayType, nButton, keystroke );
        }
    } 
    
     /**
     * notifies listeners list the memory dump
     * @param nodeId identifies the network element
     * @param memory refers to the value (Decimal) of the six next bytes to the given memory address
     */
    public void fireOnGetMemory( String nodeId, String memory )   
    {
        for( DPEvents listener: listeners ){
            listener.onGetMemory(nodeId, memory);
        }
    }   
    
     /**
     * notifies listeners list a network distribution
     * @param channel1 nodes in the channel1
     * @param channel2 nodes in the channel2
     * @see DPNode
     */
    public void fireOnNetworkDistribution( ArrayList<DPNode> channel1, ArrayList<DPNode> channel2  )   
    {
        for( DPEvents listener: listeners ){
            listener.onNetworkDistribution(channel1, channel2);
        }
    }      
    
     /**
     * notifies listeners list a set network distribution ack
     * @param errorNumber the value of errorNumber
     * @see DPClt
     */
    public void fireOnSetNetworkAck( String errorNumber )
    {        
        String errorDescription= "Unknown error";
        for( DPEvents listener: listeners ){         
            switch ( Integer.valueOf(errorNumber) ){
                case 0x39:
                    errorDescription="ACK OK – Configuration transmitted OK";
                    break;
                case 0x34:
                    errorDescription="ACK ERROR – Frame with wrong format";
                    break; 
                case 0x36:
                    errorDescription="ACK ERROR – Impossible to store content";
                    break;                     
            }
                
            listener.onSetNetworkACK( Integer.valueOf(errorNumber), errorDescription);
        }
    }     
    
    /**
     * notifies listeners list a alarm error
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param errorNumber the value of errorNumber
     */
    public void fireOnAlarmReception( String nodeId, String channel, String errorNumber )
    {        
        String errorDescription= "Unknown error";
        char cError;
        cError = (char) ((Integer.valueOf(errorNumber).byteValue())+'0');
        for( DPEvents listener: listeners ){         
            switch ( cError ){
                case 0x31:
                    errorDescription="Comunication Ring on one channel is broken";
                    break;
                case 0x32:
                    errorDescription="Node detected as disconnected";
                    break;
                case 0x33:
                    errorDescription="Node detected with hardware problems";
                    break;                    
                case 0x34:
                    errorDescription="Frame with wrong format";
                    break; 
                case 0x36:
                    errorDescription="Impossible to transmit the frame to node";
                    break;                     
            }
                
            listener.onAlarmReception(nodeId, channel, Integer.valueOf(errorNumber), errorDescription);
        }
    }       

    void fireScanReception(String nodeId, String channel, String scan ) 
    {
        for( DPEvents listener: listeners ){
            listener.onScanReception(nodeId, channel, scan);
        }
    }
        
    public void fireOnPrintDisplayACK(ByteBuffer msg, String nodeId, String channel, int errorNumber ) 
    {
        String errorDescription= "Unknown error";
        char cError;
        cError = (char) ((Integer.valueOf(errorNumber).byteValue())+'0');
        for( DPEvents listener: listeners ){         
            switch ( cError ){
                case 0x39:
                    errorDescription="ACK OK – Transmission was done";
                    break;
                case 0x32:
                    errorDescription="ACK ERROR – Node detected as disconnected";
                    break;
                case 0x33:
                    errorDescription="ACK ERROR – Node detected with hardware problems";
                    break;                    
                case 0x34:
                    errorDescription="ACK ERROR – Frame with wrong format";
                    break; 
                case 0x35:
                    errorDescription="ACK ERROR – Alarm when trying to send data to nodes not configured in DPI interface";
                    break;
                case 0x36:
                    errorDescription="ACK ERROR – Impossible to transmit frame to node";
                    break;                     
            }
            listener.onPrintDisplayACK(msg, nodeId, channel, errorNumber, errorDescription);
        }
        
    }
    
}
