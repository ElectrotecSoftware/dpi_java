/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2015 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2015 Sun Microsystems, Inc.
 */

package electrotec;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.InterruptedByTimeoutException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements the technical details of a 
 * communication protocol in a DP Series displays network.
 * @author      MJM
 * @version     1.0
 * @since       1.0
 */
public class DPClt implements Runnable 
{
    /**
     * RGB color combination.
     */    
    public enum LEDColor{

        /**
         * 0,0,0
         */
        ledOff (0), 

        /**
         * 1,0,0
         */
        Red    (4), 

        /**
         * 0,1,0
         */
        Green  (2), 

        /**
         * 0,0,1
         */
        Blue   (1), 

        /**
         * 1,1,1
         */
        White  (7), 

        /**
         * 1,1,0
         */
        Yellow (6), 

        /**
         * 0,1,1
         */
        Cyan   (3), 

        /**
         * 1,0,1
         */
        Magenta(5);        
        
        private final String color;
        
        LEDColor(int paramColor){
            String binary  = Integer.toBinaryString(paramColor);
            String tmpColor="";
            binary="000".substring(binary.length())+binary;
            
            for( int i=0; i < 3; i++ ){
                if( binary.length() > i )
                    tmpColor+= binary.substring(i,i+1);
                else
                    tmpColor+= "0";
                
                if( i < 2)
                    tmpColor +=",";
            }
            color= tmpColor;
        }
        
        @Override
        public String toString()
        {
            return color;
        }
        
        public static LEDColor getEnum(String name) 
        {
             if( name.equals("RED"))
                return LEDColor.Red;           
 
             if( name.equals("GREEN"))
                return LEDColor.Green;    

             if( name.equals("BLUE"))
                return LEDColor.Blue;  

             if( name.equals("CYAN"))
                return LEDColor.Cyan;    
             
             if( name.equals("MAGENTA"))
                return LEDColor.Magenta;   
             
             if( name.equals("WHITE"))
                return LEDColor.White;   
             
             if( name.equals("YELLOW"))
                return LEDColor.Yellow;      
             
             if( name.equals("OFF"))
                 return LEDColor.ledOff;
             
             return LEDColor.ledOff;
        }        

    }  

    /**
     * Led blink mode.
     */    
    public enum BlinkMode{
        /**
         * No blink
         */
        noBlink(0), 
        /**
         * blink even 0,25 seconds
         */
        quarter(1), 

        /**
         * blink even 0,5 seconds
         */
        half(2), 

        /**
         * blink even 1 seconds
         */
        second(4);
           
        private final String blink;
        
        BlinkMode(int paramBlink){
            char text = (char) (((char)paramBlink)+'0');
            blink = ""+text;            
        }

        @Override
        public String toString()
        {
            return blink;
        }        
        
        public static BlinkMode getEnum(String name) 
        {
             if( name.equals("HALF"))
                return BlinkMode.half;           
 
             if( name.equals("QUARTER"))
                return BlinkMode.quarter;      
             
             if( name.equals("SECOND"))
                return BlinkMode.second;                 

             return BlinkMode.noBlink;
        }         
    }  
    
    /**
     * Key sound.
     */    
    public enum KeySound{

        /**
         * deactivate the key beep
         */
        deactivate(0), 

        /**
         * activate the key beep
         */
        activate(1);
           
        private final String sound;
        
        KeySound(int paramSound){
            char text = (char) (((char)paramSound)+'0');
            sound = ""+text;
        }
        
        @Override
        public String toString()
        {
            return sound;
        }        
        
        public static KeySound getEnum(String name) 
        {
             if( name.equals("ACTIVATE"))
                return KeySound.activate;           
 

             return KeySound.deactivate;
        }         

    }      
    
    /**
     * Make Beep.
     */    
    public enum MakeBeep{
        /**
         * no beep
         */
        noBeep(0), 
        /**
         * single beep
         */
        singleBeep(1), 

        /**
         * double beep (short-short)
         */
        shortShort(2),
        
        /**
         * double beep (short-long)
         */
        shortLong(3);        
           
        private final String sound;
        
        MakeBeep(int paramSound){
            char text = (char) (((char)paramSound)+'0');
            sound = ""+text;
        }

        @Override
        public String toString()
        {
            return sound;
        }         
        
        public static MakeBeep getEnum(String name) 
        {
             if( name.equals("SHORT-LONG"))
                return MakeBeep.shortLong;           
 
             if( name.equals("SHORT-SHORT"))
                return MakeBeep.shortShort;       
             
             if( name.equals("SINGLE BEEP"))
                return MakeBeep.singleBeep;    
             
             return MakeBeep.noBeep;
        }        
        
    }     
    
    /**
     * Relay outputs.
     */    
    public enum RelayOutputs{

        /**
         * deactivate the relay output
         */
        deactivate(0), 

        /**
         * activate the relay output
         */
        activate(1);
           
        private final String relay;
        
        RelayOutputs(int paramRelay){
            char text = (char) (((char)paramRelay)+'0');
            relay = ""+text;            
        }

    }   
    
    /**
     * status connection.
     */    
    public enum ConnectionStatus{
        UNINITIALIZED(0), 
        DISCONNECTING(1), 
        DISCONNECTED(2), 
        CLOSED(3), 
        CONNECTING(4),        

        /**
         * Connected to the server
         */
        CONNECTED(5); 
        private final int value;

        private ConnectionStatus(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }        
    }   
    
    /**
     * Conenction status
     */  
    protected ConnectionStatus status;
    
    /**
     * Phase telegram type recognition.
     */
    private final static byte COMMAND               = 2;

    /**
     * Phase separator command recognition
     */
    private final static byte SPLITTERCOMMAND       = 3;

    /**
     * Message recognition phase
     */
    private final static byte MSG                   = 4;
    
    /**
     * Message of network distribution recognition phase
     */
    private final static byte MSGNETWORKDISTRIBUTION     = 5;    
    /**
     * Message of network distribution recognition phase
     */
    private final static byte MSGNETNODEID     = 6;     
    /**
     * Message of network distribution recognition phase
     */
    private final static byte MSGNETNODESTATUS     = 7;       
    /**
     * Message of network distribution recognition phase
     */
    private final static byte MSGNETNODETYPE     = 8;  
    /**
     * Message of network distribution recognition phase
     */
    private final static byte MSGNETNOMORE     = 9;     
    
    /**
     * Telegram start
     * 
     */
    public final static byte TELEGRAMSTRUCTURE_BEGINNING = 0x02;

    /**
     * Field splitter
     */
    public final static byte TELEGRAMSTRUCTURE_SPLITTER = 0x05;
    
    /**
     * Field data splitter
     */
    public final static byte TELEGRAMSTRUCTURE_DATASPLITTER = 0x2c;    

    /**
     * Telegram end
     */
    public final static byte TELEGRAMSTRUCTURE_FINAL = 0x03;    
    
    /**
     * Recognition phase the start of the telegram
     */
    private final static byte BEGINNINGOFTHEMESSAGE = 1;
    
    /**
     * Started by the client, this telegram asks the server to send 
     * the protocol version. It has no data in the data block.
     * 
     * @see #sendServerVersionRequest() 
     */
    public final static byte COMMAND_VERSIONREQUEST = 0x31;

    /**
     * Started by the client, this telegram opens a session with server. 
     * It must be done before sending other frames. The server will return 
     * the number of current working sessions.
     * The server returns the number of active lines. If the session can’t 
     * be activated it returns a number 9 on Active sessions field.
     * 
     * @see #openSession() 
     * @see DPEvents#onOpenSession(java.lang.String) 
     */
    public final static byte COMMAND_OPENSESSIONREQUEST = 0x32;

    /**
     * Started by the server, this telegram is sent to tell the kind 
     * of error produced and the involved node.
     * If the error does not involve a node, node 0 is sent. 
     * <P>
<table summary="">
     <tr><th>Err Number</th><th>Abbreviation</th><th>Description</th></tr> 
     <tr><td>0x31</td><td>RING BROKEN</td><td>Comunication Ring on one channel is broken</td></tr>
     <tr><td>0x32</td><td>NODE DISCONNECTED</td><td>Node detected as disconnected</td></tr>
     <tr><td>0x33</td><td>HARDWARE ERROR</td><td>Node detected with hardware problems</td></tr>
     <tr><td>0x34</td><td>WRONG FORMAT</td><td>Frame with wrong format</td></tr>
     <tr><td>0x35</td><td>NODE NOT CONFIGURED</td><td>Alarm when trying to send data to nodes not configured in DPI interface</td></tr>
     <tr><td>0x36</td><td>TRANSMISSION FAIL</td><td>Impossible to transmit the frame to node</td></tr>
</table>
     * 
     * @see DPEvents#onAlarmReception(java.lang.String, java.lang.String, int, java.lang.String) 
     */
    public final static byte COMMAND_ALARMRECEPTION = 0x33;

    /**
     * Started by the client, this telegram asks the server to print a 
     * text in the display digits, illuminating lights with the chosen 
     * color and blinking and setting the buzzer sound as well.
     * 
     * @see #printDisplay(java.lang.String, java.lang.String, java.lang.String, electrotec.DPClt.LEDColor, electrotec.DPClt.BlinkMode, electrotec.DPClt.KeySound, electrotec.DPClt.MakeBeep) 
     * 
     */
    public final static byte COMMAND_DISPLAYPRINT = 0x34;

    /**
     * Started by the client, it requests the server to manage the three relay outputs of DPI interface.
     * @see #setRelayOutput(electrotec.DPClt.RelayOutputs, electrotec.DPClt.RelayOutputs, electrotec.DPClt.RelayOutputs) 
     */
    public final static byte COMMAND_MANAGINGRELAYOUTPUTS = 0x35;

    /**
     * Started by the server, it sends to all connected clients the info about the pressed keys on the displays. 
     * It encloses the display number, the communication channel, the kind of display and which key has been pressed. 
     * It shows the symbol, that may be “V”, “+”, “-“ or “F”, or a combination of the 4 keys in displays DPA and DPM.
     * @see DPEvents#onKeyStroke(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) 
     */
    public final static byte COMMAND_KEYPRESSEDRECEPTION = 0x36;

    /**
     * Started by the client, it requests the server to put the sent data starting at the given memory address.
     * @see #setMemory(java.lang.String, java.lang.String, java.lang.String) 
     */
    public final static byte COMMAND_SETMEMREQUEST = 0x37;

    /**
     * Started by the client, it requests the server to send the six next bytes that follow the given memory address.
     * @see DPEvents#onGetMemory(java.lang.String, java.lang.String) 
     * @see #getMemory(java.lang.String, java.lang.String, java.lang.String) 
     */
    public final static byte COMMAND_GETMEMREQUEST = 0x38;

    /**
     * The node network scheme is stored on DPI side using to allow DPI knowing which types of nodes
     * are connected on the network and to give alarms related to its operation.
     * To know the node network configuration of DPI it’s necessary to request them using the 
     * following command. 
     * @see DPEvents#onNetworkDistribution(java.util.ArrayList, java.util.ArrayList) 
     * @see #sendNetworkDistributionRequest() 
     */
    public final static byte COMMAND_GETDISTRIBUTIONREQUEST= 0x39;

    /**
     * This command stores contend to microSD. The repeated use of this function 
     * can kill the stored content on microSD and cause malfunction of DPI interface.
     * This function lets you to transfer to DPI a network scheme. The network scheme will be stored in 
     * microSD for future use. 
     * The node network scheme is can also be configured on DPI side using its valintegrated webserve
     * @see DPEvents
     */
    public final static byte COMMAND_SETDISTRIBUTION       = 0x3a;

    /**

     * @see DPEvents
     */
    public final static byte COMMAND_PROGRAMNODEID         = 0x3b;

    /**
     * Started by the server, sends to all connected clients the info received on DPW modules. It encloses 
     * the node number, the communication channel and the collected DATA.
     * @see DPEvents
     */
    public final static byte COMMAND_DPWRECEPTION          = 0x40;

    /**
     * Started by the client, this telegram asks the server to print 
     * a text in the display digits, illuminating lights with the 
     * chosen color and blinking and setting the buzzer sound as well.
     * Server returns acknowledge of Display print request. If the 
     * ACK does not involve a node, node 0 is sent.
     * @see #printDisplayAR(java.lang.String, java.lang.String, java.lang.String, electrotec.DPClt.LEDColor, electrotec.DPClt.BlinkMode, electrotec.DPClt.KeySound, electrotec.DPClt.MakeBeep) 
     */
    public final static byte COMMAND_DPANSWEREDREQUEST     = 0x42;

    /**
     *
     */
    private Charset           charset     = Charset.forName("UTF-8");     

    /**
     *
     */
    private InetSocketAddress address     = null;

    /**
     *
     */
    private SocketChannel     channel     = null;
    
    /**
     *
     */
    private final Queue<ByteBuffer> out           = new LinkedList<>();    
    private final ArrayList<ByteBuffer> poolACK   = new ArrayList<>();
    private final DPEventsAdapter   eventsAdapter = new DPEventsAdapter();    
    private int               msgId         = 1;

    /**
     *
     */
    private int               timeoutOpen = 1000;

    /**
     *
     */
    private int               timeoutSend = 100;

    /**
     *
     */
    private int               sleepOpen   = 50;

    /**
     *
     */
    private String            host        = "";

    /**
     *
     */
    private int               port        = 0;

    /**
     *
     */
    private Selector          selector    = null;

    /**
     *
     */
    private SelectionKey      serverkey   = null;   

    private long startTime;    
   
    /**
     *
     */
    public DPClt( ) 
    {
        status = DPClt.ConnectionStatus.DISCONNECTED;
        poolACK.ensureCapacity(250);
        for( int i=0; i<250;i++)
            poolACK.add(null);
        startTime=0;
    }    
    
    @Override
    public void run()
    {
        ByteBuffer buffer;
        long       startTimeLocal=0;
        try{
            
            while( true ){
                if( status == DPClt.ConnectionStatus.CONNECTING ){                 
                    this.finishConnect();
                }
                if( status == DPClt.ConnectionStatus.CONNECTED ){
                    if( !channel.isConnected() ){
                        throw new java.nio.channels.ClosedSelectorException();
                    }
                    listen();
                    if( System.currentTimeMillis() - startTimeLocal> this.timeoutSend ){
                        synchronized(out) {
                            buffer = out.poll();
                        }
                        if( buffer != null){
                            channel.write(buffer);
                            if (buffer.remaining() > 0) {
                                // ... or the socket's buffer fills up
                                break;
                            }                            
                            buffer.clear();
                            buffer=null;
                        }
                        startTimeLocal = System.currentTimeMillis();
                    }
                }
                if( status == DPClt.ConnectionStatus. CLOSED){                 
                    break;
                }                 
            }
    
        }catch(java.nio.channels.ClosedSelectorException | java.nio.channels.InterruptedByTimeoutException t)
        {
            eventsAdapter.fireOnDisconnect();
            eventsAdapter.fireOnError(t);
        }catch( Exception e )
        {
            try {
                close();
            } catch (Exception ex) {
                Logger.getLogger(DPClt.class.getName()).log(Level.SEVERE, null, ex);
            }
            eventsAdapter.fireOnError( e );
        }
    }
  

    /**
     *
     * @param buffDest
     * @param pos
     * @param sep
     * @return
     */
    public String getParam( byte buffDest[], int pos, char sep )
    {
        String tmp="";
        
        for( int i = pos ; i< buffDest.length && ( buffDest[i] != sep && buffDest[i] != 3) ; i++ ){
            tmp += (char)buffDest[i];
        }
        
        return tmp;
    }
    
    /**
     *
     * @return
     */
    public String getHost()
    {        
        return address.getHostName();
    }
    
    /**
     *
     * @return
     */
    public int getPort()
    {        
        return address.getPort();
    }
    
    /**
     *
     * @param timeout
     */
    public void setTimeoutOpen( int timeout )
    {
        timeoutOpen = timeout;
    }
    
    /**
     *
     * @param sleepTime
     */
    public void setSleepOpen( int sleepTime )
    {
        sleepOpen = sleepTime;
    } 
    
    /**
     *
     * @param host server
     * @param port port
     * @throws Exception
     */
    public void connect( String host, int port ) throws Exception
    {
        this.host = host;
        this.port = port;
        
        address   = new InetSocketAddress (host, port);

        if( address == null )
            throw new java.nio.channels.UnresolvedAddressException();
                    
        if( channel != null && channel.isOpen()  ){ 
            close();
        }
        
        channel = SocketChannel.open();       
        channel.configureBlocking(false);
        channel.connect(address);
        
        status    = DPClt.ConnectionStatus.CONNECTING;
        startTime = System.currentTimeMillis();

        selector  = Selector.open();
        serverkey = channel.register( selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ | SelectionKey.OP_WRITE  );
       
       
    }  
    
     
    /**
     *
     * @return
     * @throws Exception
     */
    public boolean finishConnect() throws Exception
    {        
        boolean finish = channel.finishConnect();

        if (!finish) {
            if (System.currentTimeMillis() - startTime< this.timeoutOpen) {
                Thread.sleep(sleepOpen);
            } else {
                throw new InterruptedByTimeoutException();
            }
        }else{
            if( finish && status.getValue() <= ConnectionStatus.CONNECTING.getValue() ){
                status = ConnectionStatus.CONNECTED;
                this.eventsAdapter.fireOnConnect();
            }
        }
        return finish;
    }    
        
    /**
     *
     * @throws Exception
     */
    public synchronized void close() throws Exception
    {       
        if( status  != ConnectionStatus.CONNECTED ){
            return ;
        }        
        status = DPClt.ConnectionStatus.DISCONNECTING;
        
        if( serverkey != null && serverkey.isValid() ){
            serverkey.cancel();
            serverkey = null;
        }
        
        if( selector != null && selector.isOpen() ){
            selector.close(); 
            selector = null;
        }
               
        if( channel != null ) {
            if( channel.isConnected() ) channel.close();
            channel = null;
            serverkey = null; 
            selector = null;            
        }        
        
        this.eventsAdapter.fireOnDisconnect();

        status = DPClt.ConnectionStatus.CLOSED;
    }    
    
    /**
     *
     * @return
     */
    public boolean isConnected()
    {
        if( channel == null ) return false;
        
        return channel.isConnected();
    }
     
    /**
     *
     * @return
     * @throws Exception
     */
    public boolean listen( ) throws Exception
    {
        SelectionKey      oKey;
        Set               oKeys ;
        Iterator          i ;
        
        // Miramos si tenemos algo en la cola
        int readyChannels = selector.select();
        
        if( readyChannels==0 ) return true;
        
        // Obtengo la listqa de eventos registrados en la cola
        oKeys = selector.selectedKeys();
        
        // Proceso la cola de eventos registrados
        for( i = oKeys.iterator(); i.hasNext(); ){
            oKey = (SelectionKey) i.next();
            
            i.remove();
            
            // Si el evento registrado es el de objetos
            if( oKey == serverkey ){
                // Si hay algo para leer
                if( oKey.isReadable() ){
                    return recvMsg();          
                }  
                if( !oKey.isValid() ){
                    throw new java.nio.channels.ClosedSelectorException();
                }
            }            
        }     
        return false;
    }    

    /**
     *
     * @param command
     * @param values
     * @throws Exception
     */
    public void sendMsg( byte command, String values[] ) throws Exception
    {  
        sendMsgACK(command,values,false);
    }
    
    /**
     *
     * @param command
     * @param values
     * @param withACK
     * @throws Exception
     */
    public void sendMsgACK( byte command, String values[], boolean withACK ) throws Exception
    {  
        if( channel == null || !channel.isConnected() ) this.connect(host,port);
       
        ByteBuffer buffer ;
        int        counter=4, i;
        
        for ( i=0; values != null && i<values.length; i++){
            counter += values[i].length();
            if( i < values.length-1 )
                counter++;
        }

       
        buffer = ByteBuffer.allocate(counter);
        buffer.put(DPClt.TELEGRAMSTRUCTURE_BEGINNING );
        buffer.put( command );
        buffer.put(DPClt.TELEGRAMSTRUCTURE_SPLITTER );

        for ( i=0; values != null && i<values.length; i++){
            buffer.put( values[i].getBytes() );
            if( i < values.length-1 )
                buffer.put(DPClt.TELEGRAMSTRUCTURE_SPLITTER );
        }
                
        buffer.put(DPClt.TELEGRAMSTRUCTURE_FINAL );
        
        buffer.flip();
        
        // Añado el telegrama a la cola
        addQueueOut(buffer);
        
        // Si lleva ACK lo almaceno
        if( withACK ){
            try{
                poolACK.set(this.msgId, buffer);
            }catch( Exception e){
                StackTraceElement[] stackTrace = e.getStackTrace();
                System.out.println(Arrays.toString(stackTrace));
            }
        }
    }
    
    public void addQueueOut(ByteBuffer msg)
    {    
        synchronized(out) {
            out.add(msg);
        }    
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public boolean recvMsg( ) throws Exception
    {       
        if( channel == null || !channel.isConnected() ){ 
            throw new java.nio.channels.ClosedSelectorException();
        }

        ByteBuffer buffer     = ByteBuffer.allocate(1024*2);
        byte       statusMsg  =BEGINNINGOFTHEMESSAGE,position=0, command=0;
        ArrayList<String> values = new ArrayList<>();
        ArrayList<ArrayList<DPNode>> channels = new ArrayList<>();        
        DPNode node = new DPNode();
        int    channelPos=0;
        int    channelLength=0;

        String    lastMsg = "";
        
        channels.add(new ArrayList<DPNode>());
        channels.add(new ArrayList<DPNode>());

        buffer.clear();
        
        int numBytesRead = channel.read(buffer);

        if( numBytesRead == -1 ) return false;

        buffer.flip();
        
        while( buffer.remaining() > 0 ){
            position = buffer.get();
            switch( statusMsg   ){
                case DPClt.BEGINNINGOFTHEMESSAGE:
                    if( position == DPClt.TELEGRAMSTRUCTURE_BEGINNING ){
                        statusMsg   = DPClt.COMMAND; 
                    }
                    break;
                case DPClt.COMMAND:
                    command = position;
                    statusMsg   = DPClt.SPLITTERCOMMAND;
                    break;
                case DPClt.SPLITTERCOMMAND:
                    if ( command == DPClt.COMMAND_GETDISTRIBUTIONREQUEST )
                        statusMsg   = DPClt.MSGNETWORKDISTRIBUTION;
                    else                   
                        statusMsg   = DPClt.MSG; 
                    break;
                case DPClt.MSG:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_SPLITTER:
                            values.add(lastMsg);
                            lastMsg = "";
                            break;
                        case DPClt.TELEGRAMSTRUCTURE_FINAL:
                            values.add(lastMsg);
                            lastMsg = "";
                            break;
                        default:
                            lastMsg += (char) position;
                            break;
                    }
                    break;
                case DPClt.MSGNETWORKDISTRIBUTION:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_DATASPLITTER:
                            channelPos = Integer.valueOf(lastMsg)-1;
                            lastMsg = "";
                            break;
                        case DPClt.TELEGRAMSTRUCTURE_SPLITTER:
                            channelLength =Integer.valueOf(lastMsg);
                            lastMsg = "";
                            statusMsg   = MSGNETNODEID; 
                            break;
                        default:
                            lastMsg += (char) position;
                            break;
                    }
                    break;  
                case DPClt.MSGNETNODEID:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_DATASPLITTER:
                            node.setNodeId( lastMsg );
                            lastMsg = "";
                            statusMsg   = MSGNETNODESTATUS; 
                            break;
                        default:
                            lastMsg += (char) position;
                            break;
                    }
                    break;   
                case DPClt.MSGNETNODESTATUS:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_DATASPLITTER:
                            node.setState(lastMsg);
                            lastMsg = "";
                            statusMsg   = MSGNETNODETYPE; 
                            break;
                        default:
                            lastMsg += (char) position;
                            break;
                    }
                    break;
                case DPClt.MSGNETNODETYPE:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_DATASPLITTER:
                            node.setType(lastMsg);
                            lastMsg = "";
                            break;
                        case DPClt.TELEGRAMSTRUCTURE_SPLITTER:
                            node.setType(lastMsg);             
                            node.setChannel(channelPos);
                            channels.get(channelPos).add(node);
                            node = new DPNode();
                            lastMsg = "";
                            statusMsg   = MSGNETNOMORE; 
                            break;                            
                        default:
                            lastMsg += (char) position;
                            break;
                    }
                    break;     
                case DPClt.MSGNETNOMORE:
                    switch( position ){
                        case DPClt.TELEGRAMSTRUCTURE_SPLITTER:
                            statusMsg = MSGNETWORKDISTRIBUTION; 
                            break;   
                        case DPClt.TELEGRAMSTRUCTURE_FINAL:
                            lastMsg = "";
                            break;                            
                        default:
                            statusMsg = MSGNETNODEID;
                            lastMsg  += (char) position;
                            break;
                    }
                    break;                      
            }
        }


        switch( command ){
            case DPClt.COMMAND_DPANSWEREDREQUEST:
                int pos   = values.size() >= 1 ? Integer.valueOf(values.get(0)):0;
                int error = values.size() >= 1 ? Integer.valueOf(values.get(3)):0;
                eventsAdapter.fireOnPrintDisplayACK(poolACK.get(pos), 
                        values.size() >= 2 ? values.get(1):"",
                        values.size() >= 3 ? values.get(2):"",
                        error );
                break;
            case DPClt.COMMAND_ALARMRECEPTION:
                eventsAdapter.fireOnAlarmReception(values.size() >= 1 ? values.get(0):"",
                                      values.size() >= 2 ? values.get(1):"",
                                      values.size() >= 3 ? values.get(2):"" );
                break;
            case DPClt.COMMAND_VERSIONREQUEST:
                eventsAdapter.fireOnVersion( values.size()>= 1 ? values.get(0):"" );
                break;
            case DPClt.COMMAND_OPENSESSIONREQUEST:
                eventsAdapter.fireOnOpenSession( values.size() >= 1 ? values.get(0) :"" );
                break;
            case DPClt.COMMAND_KEYPRESSEDRECEPTION:          
                String msg[]=(values.size() >= 3 ? values.get(2):"").split(",");
                eventsAdapter.fireOnKeyStroke( values.size() >= 1 ? values.get(0):"",
                                      values.size() >= 2 ? values.get(1):"",
                                      msg.length >= 1 ? msg[0]:"",
                                      msg.length >= 2 ? msg[1]:"",
                                      msg.length >= 3 ? msg[2]:"");
                break;
            case DPClt.COMMAND_GETDISTRIBUTIONREQUEST:
                eventsAdapter.fireOnNetworkDistribution(channels.get(0), channels.get(1));
                break;
            case DPClt.COMMAND_GETMEMREQUEST:
                eventsAdapter.fireOnGetMemory( values.get(0), values.get(1) );
                break;
            case DPClt.COMMAND_SETDISTRIBUTION:
                eventsAdapter.fireOnSetNetworkAck(values.get(0));
                break;
            case DPClt.COMMAND_DPWRECEPTION:
                eventsAdapter.fireScanReception( values.get(0), values.get(1), values.get(2) );
                break;
            default:    
                lastMsg = "command "+command+" not recognized [";
                for( String obj:values ){
                    lastMsg += obj;
                    lastMsg += ",";
                }
                
                if( values.size() > 0 )
                    lastMsg = lastMsg.substring(0, lastMsg.length()-1);
                
                lastMsg += "]";
                
                eventsAdapter.fireOnError( new Exception(lastMsg) );
                break;
                
        }    
       
        return true;   
    }   
    
    /**
     * print a text in the display digits, illuminating lights with the chosen color and blinking and setting the buzzer sound as well.
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param text Data text to be displayed
     * @param color Led light
     * @param blink Led blink mode
     * @param key activate or deactivate the key sound
     * @param make beep
     * @throws Exception
     */
    public void printDisplay(String nodeId, 
                             String channel, 
                             String text,
                             LEDColor color,
                             BlinkMode blink,
                             KeySound key,
                             MakeBeep make ) throws Exception
    {
        String msg[]=new String[3];

        if( text.length() == 0 )
            text = " ";
        
        msg[0] = nodeId;
        msg[1] = channel;
        text  += ","+color;
        text  += ","+blink;
        text  += ", ";  // Reserved
        text  += ","+key;
        text  += ","+make;
        
        msg[2] = text;
                
        sendMsg(COMMAND_DISPLAYPRINT,msg);
    }  
    
    /**
     *
     * @param outputOne
     * @param outputTwo
     * @param outputThree
     * @throws Exception
     */    
    public void setRelayOutput(RelayOutputs outputOne, 
                               RelayOutputs outputTwo, 
                               RelayOutputs outputThree  ) throws Exception
    {
        String msg[]=new String[1];

        msg[0] = outputOne+","+outputTwo+","+outputThree;
                
        sendMsg(DPClt.COMMAND_MANAGINGRELAYOUTPUTS,msg);
    }    
    
    /**
     *
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param memory to set
     * @throws Exception
     */    
    public void setMemory(String nodeId, 
                             String channel, 
                             String memory  ) throws Exception
    {
        String msg[]=new String[3];
        
        if( memory.length() < 3 )
            throw new Exception("setMemory: the message length can not be less than 3" );

        msg[0] = nodeId;
        msg[1] = channel;
        msg[2] = memory;
                
        sendMsg(DPClt.COMMAND_SETMEMREQUEST,msg);
    }   
    
    /**
     *
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param position
     * @throws Exception
     */    
    public void getMemory(String nodeId, 
                          String channel, 
                          String position ) throws Exception
    {
        String msg[]=new String[3];
        
        msg[0] = nodeId;
        msg[1] = channel;
        msg[2] = position;
                
        sendMsg(DPClt.COMMAND_GETMEMREQUEST,msg);
    }     
    
    /**
     *
     * @param nodeId identifies the network element
     * @param channel identifies the channel to which the node is connected
     * @param text
     * @param color
     * @param blink
     * @param key
     * @param make
     * @throws Exception
     */
    public void printDisplayAR(String nodeId, 
                             String channel, 
                             String text,
                             LEDColor color,
                             BlinkMode blink,
                             KeySound key,
                             MakeBeep make ) throws Exception
    {
        String msg[]=new String[4];
        
        if( msgId < 250 )
            msgId++;
        else
            msgId=1;
                    
        if( text.length() == 0 )
            text = " ";
        
        msg[0] = Integer.toString(msgId);
        msg[0] = "000".substring(msg[0].length())+msg[0];
        msg[1] = nodeId;
        msg[2] = channel;
        msg[3] = text+","+color+","+blink+", "+ // Reserved
                 ","+key+","+make;
                
        sendMsgACK(DPClt.COMMAND_DPANSWEREDREQUEST,msg,true);
    }     

    /**
     * ask for the protocol version
     * @throws Exception
     */
    public void sendServerVersionRequest() throws Exception
    {
        sendMsg(DPClt.COMMAND_VERSIONREQUEST, null);
    }    
    
    /**
     * ask for the network distribution
     * @throws Exception
     */
    public void sendNetworkDistributionRequest() throws Exception
    {
        sendMsg(DPClt.COMMAND_GETDISTRIBUTIONREQUEST, null);
    }      
    
    /**
     * ask for a session
     * @throws Exception
     */
    public void openSession() throws Exception
    {
        sendMsg(DPClt.COMMAND_OPENSESSIONREQUEST, null);
    }       
                      
 
    /**
     * add a listener
     * @param listener
     */
    public void addEventListener( DPEvents listener ) 
    {
        eventsAdapter.addEventListener(listener);
    }
    
    /**
     * remove a listener
     * @param listener
     */
    public void removeEventListener(DPEvents listener) 
    {
        eventsAdapter.removeEventListener(listener);
    }      
}
